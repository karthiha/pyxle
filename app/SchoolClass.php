<?php

namespace pyxle;

use Illuminate\Database\Eloquent\Model;
use pyxle\Grade;


class SchoolClass extends Model
{
    //
    protected $table = 'classes';

    // Grade and Classes has One to Many relationship
    public function grade(){
        return $this->belongsTo(Grade::class);
    }

    // Class and Students has One - to - Many relationship
    public function students(){
        return $this->hasMany(Student::class);
    }
}
