<?php

namespace pyxle;

use Illuminate\Database\Eloquent\Model;
use pyxle\SchoolClass;

class Grade extends Model
{
    protected $table = 'grades';

    // Grade and Classes has One to Many relationship
    public function classes()
    {
        return $this->hasMany(SchoolClass::class);
    }

    // Grade and Students has One to Many relationship
    public function students()
    {
        return $this->hasMany(Student::class);
    }
}
