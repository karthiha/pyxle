<?php

use League\Fractal\TransformerAbstract;
use pyxle\SchoolClass;
use pyxle\Http\Controllers\Controller;

class ClassTransformer extends TransformerAbstract
{

    public function transform(SchoolClass $class)
    {
        return [
            'id' => (int) $class->id,
            'name' => $class->grade,
            'created_at' => date_format($class->created_at,'Y-m-d H:i:s'),
            'updated_at' => date_format($class->updated_at,'Y-m-d H:i:s')
        ];
    }

}