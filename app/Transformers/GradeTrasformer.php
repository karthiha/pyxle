<?php

use League\Fractal\TransformerAbstract;
use pyxle\Grade;
use pyxle\SchoolClass;

class GradeTransformer extends TransformerAbstract
{

    protected $defaultIncludes=[
        'SchoolClasses,Students'
    ];

    public function transform(Grade $grade)
    {
        return [
            'id' => (int) $grade->id,
            'name' => $grade->grade,
            'created_at' => date_format($grade->created_at,'Y-m-d H:i:s'),
            'updated_at' => date_format($grade->updated_at,'Y-m-d H:i:s')
        ];
    }

    public function includeSchoolClasses(Grade $grade){
        $classes = $grade->classes()->get();
        if(count($classes)>0){
            return $this->collection($classes,new ClassTransformer());
        }
    }

    public function includeStudents(Grade $grade){
        $students = $grade->students()->get();
        if(count($students)>0){
            return $this->collection($students,new StudentTransformer());
        }
    }
}