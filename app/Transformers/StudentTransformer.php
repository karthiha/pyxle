<?php

use League\Fractal\TransformerAbstract;
use pyxle\Student;
use pyxle\Http\Controllers\Controller;

class StudentTransformer extends TransformerAbstract
{

    public function transform(Student $student)
    {
        return [
            'id' => (int) $student->id,
            'name' => $student->name,
            'age' => $student->age,
            'address' => $student->address,
            'dateOfBirth' => $student->dateOfBirth,
            'gender' => $student->gender,
            'phone' => $student->phone,
            'created_at' => date_format($student->created_at,'Y-m-d H:i:s'),
            'updated_at' => date_format($student->updated_at,'Y-m-d H:i:s')
        ];
    }

}