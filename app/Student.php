<?php

namespace pyxle;

use Illuminate\Database\Eloquent\Model;
use pyxle\Grade;
use pyxle\SchoolClass;

class Student extends Model
{
    // Student belongs to a Grade
    public function grade(){
        return $this->belongsTo(Grade::class);
    }

    // Student Belongs to a Class
    public function classes()
    {
        return $this->belongsTo(SchoolClass::class);
    }
}
