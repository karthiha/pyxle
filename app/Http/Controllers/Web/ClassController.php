<?php

namespace pyxle\Http\Controllers\Web;

use pyxle\Grade;
use Illuminate\Http\Request;

use pyxle\Http\Requests;
use pyxle\Http\Controllers\Controller;
use pyxle\SchoolClass;
use pyxle\Http\Requests\ClassRequest;
use Illuminate\Support\Facades\Redirect;

class ClassController extends Controller
{
    protected $schoolClass;
    protected $grade;

    public function __construct(SchoolClass $schoolClass,Grade $grade)
    {
        $this->schoolClass = $schoolClass;
        $this->grade = $grade;
    }
    /**
     * Display a listing of the Classes with related Grades.
     */
    public function index()
    {
        // All the Grades with related Classes.
        $classes = Grade::with('classes')->get();
        return view('class/index',compact('classes'));
    }

    /**
     * Show the form for creating a new Class.
     */
    public function create()
    {
        // Get all the grades
        $grades = Grade::lists('grade','id');

        // Add new class
        return view('class/create',compact('grades'));
    }

    /**
     * Store a newly created Class in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClassRequest $request)
    {
        $class = $this->schoolClass;

        $class->class = $request->class;
        $class->grade_id = $request->grade;

        $class->save();
        return Redirect::route('class.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
