<?php

namespace pyxle\Http\Controllers\Web;

use pyxle\SchoolClass;
use Illuminate\Http\Request;

use pyxle\Http\Requests;
use pyxle\Http\Controllers\Controller;
use pyxle\Student;
use pyxle\Grade;
use Illuminate\Support\Facades\DB;
use pyxle\Http\Requests\StudentRequest;
use Illuminate\Support\Facades\Redirect;

class StudentController extends Controller
{
    protected $student;

    public function __construct(Student $student)
    {
        $this->student = $student;
    }
    /**
     * Display a listing of the Students.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Return all the students details
        $students = Grade::with('students')->get();
        return view('student/index',compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Get all the grades
        $grades = Grade::lists('grade','id');
        // Get all the classes
        $classes = SchoolClass::lists('class','id');
        return view('student/create',compact('grades','classes'));
    }

    /**
     * Store a newly created Student in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $request)
    {
        $student = $this->student;
        $student->name = $request->name;
        $student->gender = $request->gender;
        $student->age = $request->age;
        $student->dateOfBirth = $request->dob;
        $student->address = $request->address;
        $student->phone = $request->phone;
        $student->grade_id = $request->grade;
        $student->class_id = $request->class;

        $student->save();

        return Redirect::route('student.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
