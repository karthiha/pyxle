<?php

namespace pyxle\Http\Controllers\Web;

use Illuminate\Http\Request;

use pyxle\Http\Requests;
use pyxle\Http\Controllers\Controller;
use pyxle\Grade;
use pyxle\Http\Requests\GradeRequest;
use Illuminate\Support\Facades\Redirect;

class GradeController extends Controller
{
    protected $grade;

    public function __construct(Grade $grade)
    {
        $this->grade = $grade;
    }
    /**
     * Display listing of all the Grades
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grades = $this->grade->all();
        return view('grade/index',compact('grades'));
    }

    /**
     * Add a new Grade
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('grade/create');
    }

    /**
     * Store a newly created Grade in storage.
     */
    public function store(GradeRequest $request)
    {
        $grade = $this->grade;
        $grade->grade = $request->grade;
        $grade->save();

        // Redirect to Grade index page
        return Redirect::route('grade.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
