<?php

namespace pyxle\Http\Controllers\API;

use pyxle\Grade;
use Illuminate\Http\Request;

use pyxle\Http\Requests;
use pyxle\Http\Controllers\Controller;
use GradeTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class GradeController extends Controller
{
    protected $grade;
    protected $manager;

    public function __construct(Grade $grade,Manager $manager){
        $this->grade = $grade;
        $this->manager = $manager;
    }

    /*
   * List all the Grades
   */
    public function index(){
        $grades = Grade::with('classes')->get();
        // Apply Transformer for Collection of data.
//        $resource = new Collection($grades,new GradeTransformer());
//        $grades = $this->manager->createData($resource)->toArray();
        return response()->json($grades);
    }
}
