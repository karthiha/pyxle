<?php

namespace pyxle\Http\Controllers\Api;

use Illuminate\Http\Request;

use pyxle\Http\Requests;
use pyxle\Http\Controllers\Controller;
use pyxle\Student;
use pyxle\Grade;
use pyxle\Http\Requests\StudentAPIRequest;

class StudentController extends Controller
{
    protected $student;

    public function __construct(Student $student)
    {
        $this->student = $student;
    }
    /**
     * Display a listing of the students.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Grade::with('students')->get();
        return response()->json($students);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentAPIRequest $request)
    {
        $student = $this->student;
        $student->name = $request->name;
        $student->gender = $request->gender;
        $student->age = $request->age;
        $student->dateOfBirth = $request->dob;
        $student->address = $request->address;
        $student->phone = $request->phone;
        $student->grade_id = $request->grade;
        $student->class_id = $request->class;

        $student->save();

        return response()->json($student);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
