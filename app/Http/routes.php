<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['namespace' => 'Api'], function() {
    // List all Classes
    Route::resource('classes','ClassController');
    // Get all the Grades with Classes
    Route::get('grades','GradeController@index');
    // Routes for Student
    Route::resource('students','StudentController');

});


Route::group(['namespace' => 'Web'], function() {
    // Routes for Class
    Route::resource('class','ClassController');
    // Routes for Grade
    Route::resource('grade','GradeController');
    // Routes for Student
    Route::resource('student','StudentController');

});


Route::auth();

Route::get('/home', 'HomeController@index');
