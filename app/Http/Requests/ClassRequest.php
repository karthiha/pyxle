<?php

namespace pyxle\Http\Requests;

use pyxle\Http\Requests\Request;

class ClassRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'class'=>'required',
            'grade'=>'required',
        ];
    }
}
