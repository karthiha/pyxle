<?php

namespace pyxle\Http\Requests;

use pyxle\Http\Requests\Request;

class StudentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'gender'=>'required',
            'age'=>'required|numeric|min:6|max:20',
            'address'=>'required',
            'dob'=>'required',
            'phone'=>'numeric|digits_between:0,10',
        ];
    }
}
