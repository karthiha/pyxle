<?php

namespace pyxle\Http\Requests;

use pyxle\Http\Requests\Request;

class GradeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Grade should be unique and numeric between 1 to 13
            'grade'=>'required|unique:grades|numeric|min:1|max:13',
        ];
    }
}
