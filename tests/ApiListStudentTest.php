<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApiListStudentTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->seeStatusCode(200)
            ->seeHeader('content-type', 'application/json');
        $students = json_decode($this->response->getContent());
        foreach($students as $student) {
            $this->assertObjectHasAttribute('id', $student, 'ID does not exists');
            $this->assertInternalType('int', $student->id);
            $this->assertObjectHasAttribute('name', $student, 'Name is empty');
            $this->assertObjectHasAttribute('gender', $student, 'Gender is empty');
            $this->assertObjectHasAttribute('dob', $student, 'Content type is empty');
            $this->assertObjectHasAttribute('age', $student, 'Path is empty');
            $this->assertInternalType('int', $student->age);
            $this->assertObjectHasAttribute('phone', $student, 'Phone is empty');
            $this->assertObjectHasAttribute('address', $student, 'Address is empty');
        }
    }
}
