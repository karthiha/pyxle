@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Grades
                        <p class="pull-right"> <a href="{!! route('grade.index') !!}" class="btn btn-success" /> Grades </a> </p></div>

                    <div class="panel-body">
                        {!! Form::open(array('route'=>'grade.store')) !!}
                        <div class="row">
                            <div class="col-md-6">
                                <md-input-container class="form-group {!! ($errors->has('grade')) ? 'has-error' : '' !!}">
                                    {!! Form::label('grade', 'Grade') !!}
                                    {!! Form::number('grade', null, ['class' => 'form-control']) !!}
                                    <span class="help-block">{!! $errors->has('grade')? $errors->first('grade'):'' !!}</span>
                                </md-input-container>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::submit('Save',array('class'=>'btn btn-success')) !!}
                                {!! link_to_route('grade.index','Cancel',[],['class' => 'btn btn-danger']) !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection