@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Grades
                    <p class="pull-right"> <a href="{!! route('grade.create') !!}" class="btn btn-success" /> Add </a> </p></div>

                    <div class="panel-body">
                        <table class="table table-responsive">
                            <tr>
                                <th>Grades</th>
                            </tr>
                            @foreach($grades as $grade)
                                <tr>
                                    <td>
                                        {{$grade->grade}}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection