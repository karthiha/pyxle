@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Grades
                        <p class="pull-right"> <a href="{!! route('class.index') !!}" class="btn btn-success" /> Classes </a> </p></div>

                    <div class="panel-body">
                        {!! Form::open(array('route'=>'class.store')) !!}
                        <div class="row">
                            <div class="col-md-6">
                                <fieldset class="form-group {!! ($errors->has('grade')) ? 'has-error' : '' !!}">
                                    {!! Form::label('grade', 'Grade') !!}
                                    {!! Form::select('grade',  $grades,null, ['class' => 'form-control']) !!}
                                    <span class="help-block">{!! $errors->has('grade')? $errors->first('grade'):'' !!}</span>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <md-input-container class="form-group {!! ($errors->has('class')) ? 'has-error' : '' !!}">
                                    {!! Form::label('class', 'Class') !!}
                                    {!! Form::text('class', null, ['class' => 'form-control']) !!}
                                    <span class="help-block">{!! $errors->has('class')? $errors->first('class'):'' !!}</span>
                                </md-input-container>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::submit('Save',array('class'=>'btn btn-success')) !!}
                                {!! link_to_route('class.index','Cancel',[],['class' => 'btn btn-danger']) !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection