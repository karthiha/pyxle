@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Classes
                        <p class="pull-right"> <a href="{!! route('class.create') !!}" class="btn btn-success" /> Add </a> </p>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>Grades</th>
                                <th>Classes</th>
                            </tr>

                            @foreach($classes as $class)
                                 <tr>
                                    <td><b>{{$class->grade}}</b></td>
                                 </tr>
                                @foreach($class->classes as $c)
                                    <tr> <td></td>
                                        <td>{{$c->class}}</td>
                                    </tr>
                                @endforeach
                            @endforeach
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection