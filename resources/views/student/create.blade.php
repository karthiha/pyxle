@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Grades
                        <p class="pull-right"> <a href="{!! route('student.index') !!}" class="btn btn-success" /> Students </a> </p></div>

                    <div class="panel-body">
                        {!! Form::open(array('route'=>'student.store')) !!}
                        <div class="row">
                            <div class="col-md-6">
                                <fieldset class="form-group {!! ($errors->has('grade')) ? 'has-error' : '' !!}">
                                    {!! Form::label('grade', 'Grade') !!}
                                    {!! Form::select('grade',  $grades,null, ['class' => 'form-control']) !!}
                                    <span class="help-block">{!! $errors->has('grade')? $errors->first('grade'):'' !!}</span>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <md-input-container class="form-group {!! ($errors->has('class')) ? 'has-error' : '' !!}">
                                    {!! Form::label('class', 'Class') !!}
                                    {!! Form::select('class',$classes, null, ['class' => 'form-control']) !!}
                                    <span class="help-block">{!! $errors->has('class')? $errors->first('class'):'' !!}</span>
                                </md-input-container>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <md-input-container class="form-group {!! ($errors->has('name')) ? 'has-error' : '' !!}">
                                    {!! Form::label('name', 'Name') !!}
                                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                    <span class="help-block">{!! $errors->has('name')? $errors->first('name'):'' !!}</span>
                                </md-input-container>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group {!! ($errors->has('gender')) ? 'has-error' : '' !!}">
                                    {!! Form::label('gender', 'Gender') !!}
                                    {!! Form::select('gender',  array('male'=>'Male','female'=>'Female'),null, ['class' => 'form-control']) !!}
                                    <span class="help-block">{!! $errors->has('grade')? $errors->first('grade'):'' !!}</span>
                                </fieldset>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <md-input-container class="form-group {!! ($errors->has('age')) ? 'has-error' : '' !!}">
                                    {!! Form::label('age', 'Age') !!}
                                    {!! Form::text('age', null, ['class' => 'form-control']) !!}
                                    <span class="help-block">{!! $errors->has('age')? $errors->first('age'):'' !!}</span>
                                </md-input-container>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group {!! ($errors->has('dob')) ? 'has-error' : '' !!}">
                                    {!! Form::label('dob', 'Date of birth') !!}
                                    {!! Form::date('dob', null, ['class' => 'form-control']) !!}
                                    <span class="help-block">{!! $errors->has('dob')? $errors->first('dob'):'' !!}</span>
                                </fieldset>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <md-input-container class="form-group {!! ($errors->has('address')) ? 'has-error' : '' !!}">
                                    {!! Form::label('address', 'Address') !!}
                                    {!! Form::text('address', null, ['class' => 'form-control']) !!}
                                    <span class="help-block">{!! $errors->has('address')? $errors->first('address'):'' !!}</span>
                                </md-input-container>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="form-group {!! ($errors->has('phone')) ? 'has-error' : '' !!}">
                                    {!! Form::label('phone', 'Phone') !!}
                                    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                                    <span class="help-block">{!! $errors->has('phone')? $errors->first('phone'):'' !!}</span>
                                </fieldset>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::submit('Save',array('class'=>'btn btn-success')) !!}
                                {!! link_to_route('student.index','Cancel',[],['class' => 'btn btn-danger']) !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection