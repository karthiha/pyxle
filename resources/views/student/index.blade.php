@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Students
                        <p class="pull-right"> <a href="{!! route('student.create') !!}" class="btn btn-success" /> Add </a> </p>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>Grade</th>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>Age</th>
                                <th>Date of birth</th>
                                <th>Address</th>
                                <th>Phone</th>
                            </tr>

                            @foreach($students as $grade)
                                {{--If Grade has Students only Display Grades--}}
                                @if(count($grade->students)>0)
                                    <tr>
                                        <td><b>{{$grade->grade}}</b></td>
                                    </tr>
                                @endif
                            @foreach($grade->students as $student)
                                <tr>
                                    <td></td>
                                    <td>{{$student->name}}</td>
                                    <td>{{$student->gender}}</td>
                                    <td>{{$student->age}}</td>
                                    <td>{{$student->dateOfBirth}}</td>
                                    <td>{{$student->address}}</td>
                                    <td>{{$student->phone}}</td>
                                </tr>
                            @endforeach
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection